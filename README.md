# SailfishOS for Xiaomi Redmi Note 2

3.0.0.8 - [![pipeline status](https://gitlab.com/sailfishos-porters-ci/hermes-ci/badges/master/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/hermes-ci/commits/master)

This repository uses gitlab-ci to build the SailfishOS images for the Xiaomi Redmi Note 2

![Xiaomi Redmi Note 2](https://cdn2.gsmarena.com/vv/pics/xiaomi/xiaomi-redmi-note-2-1.jpg "Xiaomi Redmi Note 2")

## Device specifications

Basic   | Spec Sheet
-------:|:----------
CPU     | Octa-core 2.0 GHz Cortex-A53
Chipset | Mediatek MT6795 Helio X10
GPU     | PowerVR G6200
ROM     | 16/32GB
RAM     | 2GB
Android | 5.0
Battery | 3060 mAh
Display | 1080x2160 pixels, 5.5
Rear Camera  | 13MP, PDAF
Front Camera | 5 MP



# Files

[hermes.env](https://gitlab.com/sailfishos-porters-ci/hermes-ci/blob/master/hermes.env) contains all the environment variables required for the process. On every release, version number is changed in the master branch and a new tag is created with the release number to trigger the build process.

[Jolla-@RELEASE@-hermes-@ARCH@.ks](https://gitlab.com/sailfishos-porters-ci/hermes-ci/blob/master/Jolla-@RELEASE@-hermes-@ARCH@.ks) file is the kickstart file which is used by PlatformSDK image, this files contains device specific repositories, Repositories can be changed by from devel to testing or vice versa.

[run-mic.sh](https://gitlab.com/sailfishos-porters-ci/hermes-ci/blob/master/run-mic.sh) is a simple bash script, which executes the build.

# Download

[Download the latest build](https://gitlab.com/sailfishos-porters-ci/hermes-ci/-/jobs?scope=finished)

# Source code
[https://github.com/HermesRepos](https://github.com/HermesRepos)
